# Hand Game - Grupo de Acústica y Vibraciones - Instituto para el Desarrollo Agroindusrial y de la Salud

## Funcionamiento Genreal de la Mano

La mano consta de modos de funionamiento:
1.   **Modo Fuerza** : Controla todos los dedos a traves de las contracciones musculares. La mano se cierra con la flexión y se cierra con la extensión
2.   **Modo Cilindro**: La parte inferior del pulgar se cierra automáticamente, mientras que las falanges superiores del pulgar y los demas dedos responden igual que el modo fuerza.
3.  **Modo Pinza**: Se controlan solo los dedos índice y pulgar para formar una pinza con los dedos. Se limita el cierre para que al máximo cierre queden enfrentados los dedos. La apertura y cierres se controla de la misma manera que los modos anteriores, Flexión para el cierre y Extensión para la apertura.

El cambio entre los diferentes modos de funcionamiento se realiza mediante la flexión rápida y se da en el orden especificado anteriormente. Es decir, comienza en _Modo Fuerza_ y al hacer la contracción rápida se cambia a _Modo Cilindro_. Si nuevamente se hace la contracción rápida se cambiará al _Modo Pinza_. Por último, si se realiza la flexión rápida nuevamente, se cambiará al _Modo Fuerza_ y el proceso se reinicia.

## Funcionamiento General del Juego

### Objetivo y Funcionamiento
El objetivo del juego es colocar diferentes objetos dentro de una caja de cartón centrada en el espacio. Los objetos pueden ser una pelota de tenis, un cilindro o un plano de madera. Cada uno de estos objetos están pensados para ser sujetados con cada uno de los diferentes modos de funcionamiento mencionados en la sección anterior, es decir, la pelota de tenis está pensada para ser sujetada con el _Modo Fuerza_, el cilindro de madera por el _Modo Cilindro_ y la tabla de madera por el _Modo Pinza_.

Al presionar la tecla "Enter" cualquiera de los tres objetos se creara en una ubicación aleatroria dentro del espacio.

Con el joystick se puede dirigir la mano hacia el objeto y agarrarlo mediante el cierre de la mano a traves de las señales EMG. Para arrojar el objeto a la caja solo basta con abrir la mano, nuevamente a traves de las señales EMG.

Cada sesión del juego cuenta de 6 (seis) objetos o intentos, creados de forma aleatoria con un máximo de dos(2) apariciones por cada objeto.

### Destrucción de los objetos

El objeto se destruye de tres formas posibles:
1. Arrojandolo a la caja, obteniendo un puntaje determinado porla relación entre la distancia inicial entre la mano y el objeto, y el tiempo necesario para arrojarlo.
2. Cuando se cumplan dos (2) minutos de haberse creado, arrojando un resultado de cero (0) puntos. 
3. Al caer libremente por el espacio, con puntaje cero al igual que el caso anterior.
 

### IMPORTANTE
Al inicar cada sesión, se debe hacer un movimiento de calibración de la mano, Es decir, realizar los movimientos de apertura y cierre en cada uno de los modos de funcionamiento


## Scripts

- **Carpeta *Fingers***: Cada dedo se divide en tres falanges independientes. 
    - Hay un script por cada una de las falanges. 
    - La mano se cierra cuando se recibe un valor "1" en el campo _Val_EMG_ del paquete JSON por puerto serial o cuando se presiona la tecla "x" del teclado
    - La mano se abre cuando se recibe un valor "-1" en el campo _Val_EMG_ del paquete JSON por puerto serial o cuando se presiona la tecla "w" del teclado

- **Carpeta *CreatePrefabs***: Carpeta donde se ecunetran los scripts para instranciarlos diferentes objetos. También se llama al efecto de explosion cuando se destruye el objeto.

* **GameManager.cs**: Script de control para la declaración de flags y variables auxiliares a utilizar.

* **Serial_port.cs**: Estabece la conexión serial con arduino a una tasa de 9600 baudios. Lee el paquete JSON y lo deserealiza en variables:
    - _Comando horizontal_: Controla el movimiento horizontal de la mano con el joystick
    - _Comando vertical_: Controla el movimiento vertical de la mano con el joystick
    - _Comando hand_: Controla la apertura y cierre de la mano a partir de las señales EMG
    - _Comando modo_: Controla el modo de funcionamiento de la mano a partir de la flexión rápida	
    - _Comando pulsador_: Controla la creación de un nuevo objeto

* **Game_Control.cs**: Script para control de tiempos, texto en pantalla y número de intentos

* **Hand_Control.cs**: Script de control de movimiento, apertura y cierre de la mano

* **Object_Control.cs**: Script de control del objeto. Detecta y confirma el contacto de los dedos correspondientes	(Index, Middle o Ring, al menos dos de estos más Thumb) y activa o desactiva la gravedad para que el objeto se mueva en conjunto con la mano. Detecta el flag de destrucción de objeto, lo destruye, controla los flags correspondientes, y establece los datos y puntajes obtenidos	
	
* **Escritura_Puntos.cs**: Script exclusivo para escribir las estadísticas de la sesión en un archivo de excel. Se debe cambiar el path por cada sujeto que realice la prueba. Los datos se escriben al final del archivo, no se sobreescriben. Los datos que se escriben son:
    - Tiempo: Desde que se presiona el enter hasta que se destruye el objeto
    - Distancia: Desde la ubicación del objeto hasta la ubicación de la mano
    - Éxtio: 1 si el objeto se destruyó por contacto con la caja 0 si se destruyó por contacto con el piso inferior (cae al vacío) o por time out (120 segundos)
    - Puntos: Puntos = _1000 * ( Distancia / Tiempo ) * Éxito_ (El 1000 es para que queden números del orden de la decena)
    - Modo de Funcionamiento: Modo de funcionamiento de la mano al momento que se destruye el objeto
    - Objeto Sujetado: Objeto que se sujetó y destruyó. Item para comparar con el modo de funcionamiento

## Arduino

En la carpeta Arduino se encuentran 3 sketchs:

- __EMG_Plotter.ino__: Su única función es leer y enviar por puerto serial los datos de los puertos A0 y A1. Su utilidad es la de chequear que los niveles de señal EMG son correctos y también para asignar ganancias a los niveles de entrada

- __EMG_Game_Setup.ino__: Realiza el Setup de los valores máximos de flexión y extensión y calcula el valor de umbral de la derivada para el cambio de modo de funcionamiento.
    - Determinación de Flexión y Extensión máxima: Luego de un parpadeo de luces led, se le pide al usuario que mantenga la contracción por momento. Durante ese tiempo se almacenan 150 muestras de los valores de contracción y se calcula el valor promedio. Los umbrales para la extensión y flexión se definen entre el 
60-70% de estos valores máximos.
    - Determinación del valor de la derivada: Luego de un parpadeo de luces led, se le pide al usuario que realice una flexión rápida. Este proceso se realiza 3 veces, y en cada intento se toman 150 muestras de los valores de contracción y se toma el valor máximo. El umbral para la derivada se determina como el 70% del valor promedio de los valores máximos de cada contracción rápida.

- __EMG_ControlSD_v2.ino__: En este scketch,  tanto los valores máximos de flexión y extrensión como el valor de umbral de derivada se deben establecer de forma manual dentro del Setup, los mismos se determinan mediante el sketch _EMG_Game_Setup.ino_.

    Una vez finalizada la etapa de Setup, el loop principal realiza las tareas de lectura de señales EMG y los compara con los diferentes umbrales para determinar el comando a enviar.
    - En primera instancia se calcula la derivada de la señal correspondiente a la flexión mediante el cálculo _(flex - flex0) / (t - t0)_, siendo _flex_ el valor de la señal, _flex0_ el valor anterior de la señal, _t_ es el tiempo actual y _t0_ el tiempo anterior. Una vez obtenido el valor de la derivada, se lo compara con el umbral establecido anteriormente y si es mayor a este, se lo toma como un posible comando switch. Luego, se compara la diferencia este valor de derivada con el valor de la derivada de 2 frames posteriores, con el 110% del valor de umbral de derivada, si es superior a este valor se cambia el modo de funcionamiento de la mano.    
    - Si el valor de la derivada es menor al umbral establecido, se procede a comparar las señales de flexión y de extensión con sus correspondientes umbrales y en caso que el valor de la señal sea mayor a dicho umbral se establece el comando de flexión o extensión según corresponda. Si los valores de las señales no superan los umbrales, el comando de acción de la mano se mantiene en cero.
    
    También, se realiza la lectura de las dos señales analógicas del joistick que controla la posición de la mano en los ejes _X_ e _Y_. Una vez que se hace la lectura de las señales se las compara con diferentes umbrales para determinar el comando de acción de movimiento. Dichos comandos pueden tomar el valor de _1_ para un moviento positivo en los ejes, _-1_ para el moviemiento negativo y _0_ para estado de reposo. 

    Luego, se hace la lectura del pulsador que se utiliza para la creación de un nuevo objeto en el juego. El pin digital cuenta con una rutina antirrebote, y si este pin tiene un valor lógico _HIGH_, la variable _sal_pulsador_ se establece en 1, en camibo, si tiene un valor lógico _LOW_, _sal_pulsador_ se establece en 0. 
 
    Además, se arma un paquete JSON con los suguientes campos:
    - _Val_x_: Comando de control de movimiento de la mano en el eje _X_
    - _Val_y_: Comando de control de movimiento de la mano en el eje _Y_
    - _Val_EMG_: Comando de control de apertura y cierre de la mano
    - _Val_Modo_: Comando de control del modo de funcionamiento de la mano
    - _Val_Puls_: Comando de control de activación de objeto

    Una vez establecido el paquete JSON se lo envía por puerto serial hacia la PC.

    Por último, se guardan en un archivo .xls en la tarjeta SD los siguientes datos:
    - Señal EMG - Flex
    - Señal EMG - Ext
    - Valor derivada
    - Modo de funcionamiento
    - Valor Pulsador
    - Extensión máxima
    - Flexión máxima
    - Umbral Extensión
    - Umbral Flexión
    - Umbral derivada

    Las funciones que se encargan del guardado de estos datos son las siguientes:
    - __(void) intializeFileDatos (String FileName)__ : Se encarga de inicializar o crear el archivo donde se gardarán los datos y escribe la etiqueta de los datos en la primer fila. El parámetro de entrada _FileName_ es el nombre del archivo a crear o incializar.

    - (void) saveData (String dataString, String FileName) : Se encarga de escribir los datos en archivo especificado. Los parámetros de entrada son los siguientes: 
        - _dataString_: cadena string que contiene todos los datos separados por un tab ("\t")
        - _FileName_: nombre del archvio donde se escriben los datos.
        
## Matlab
Los datos guardados en los archivos excel (.xls) se pueden procesar con los siguientes scripts:
- __plot_data.m__ : Hace lectura de datos y los procesa para graficar las curvas de las señales EMG
- __calc_frecuencia_muestreo.m__: Hace el calculo de la frecuencia de muestreo de los datos para poder calcular el tiempo entre muestras y así calcular el esfuerzo muscular.
- __calc_esfuerzo_muscular.m__: Calcular el esfuerzo muscular normalizado respecto al esfuerzo muscular estimado al momento del setup.

## Unity 3D
Versión de Unity: 2020.3.24f1
