clear
close all
clc

%%
%% Lectura de datos del archivo excel
%%
dataEMG = xlsread('id06/Data_id06.xlsx', 'Hoja6');
data_flex = dataEMG(:,1);
data_ext = dataEMG(:,2);
data_derivada = dataEMG(:,3);
%data_act = dataEMG(:,4);
data_modo = dataEMG(:,4);
data_pulsador = dataEMG(:,5);

umbral_ext = dataEMG(1,6)*dataEMG(1,8);
umbral_flex = (dataEMG(1,7)*dataEMG(1,9));
umbral_Derivada = dataEMG(1,10);
%frec_muestreo = dataEMG(1,11);

%%
%% Buscador de indices en modo activado
%%
%vect_index_ext = find(data_act == 1);
%vect_index_flex = find(data_act == 2);
vect_index_pulsador = find(data_pulsador == 1);

%%
%% Corrección de error de escritura
%%
for i=1:1:length(data_flex(:,1))
    if data_flex(i,1) > 5
        data_flex_act(i,1) = data_flex(i,1);
    else
        data_flex_act(i,1) = data_flex_act(i-1,1);
    end
end

for i=1:1:length(data_ext(:,1))
    if data_ext(i,1) > 5
        data_ext_act(i,1) = data_ext(i,1);
    else
        data_ext_act(i,1) = data_ext_act(i-1,1);
    end
end


for j=1:1:6
    cant_muestras(j)=length(data_flex(vect_index_pulsador(j):vect_index_pulsador(j+1)))    
end


    
    