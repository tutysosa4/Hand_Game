clear all
close all
clc

%%
%% Lectura de datos del archivo excel
%%
dataEMG = xlsread('id04/20220505_id04.xlsx', 'Hoja1');
data_flex = dataEMG(:,1);
data_ext = dataEMG(:,2);
data_derivada = dataEMG(:,3);
data_act = dataEMG(:,4);
data_modo = dataEMG(:,5);
data_pulsador = dataEMG(:,6);

umbral_ext = dataEMG(1,7)*dataEMG(1,9);
umbral_flex = dataEMG(1,8)*dataEMG(1,10);
umbral_Derivada = dataEMG(1,11);


%%
%% Lectura de valores de se�al superiores al umbral
%%
for i=1:1:length(data_flex(:,1))
    vect_umbral_flex(i) = umbral_flex;
    vect_derivada(i) = umbral_Derivada;
    if data_flex(i,1) > umbral_flex
        data_flex_act(i,1) = data_flex(i,1);
    else
        data_flex_act(i,1) = 0;
    end
end

for i=1:1:length(data_ext(:,1))
    vect_umbral_ext(i) = umbral_ext;
    if data_ext(i,1) > 5
        data_ext_act(i,1) = data_ext(i,1);
    else
        data_ext_act(i,1) = data_ext_act(i-1,1);
    end
end

for i=1:1:length(data_modo(:,1))
    if ((data_modo(i,1) == 0) || (data_modo(i,1) == 1) || (data_modo(i,1) == 2))
        data_modo_act(i) = data_modo(i,1);
    else
        data_modo_act(i)= data_modo_act(i-1);
    end
end




%%
%% Grafico de se�ales
%%
figure(1)
subplot(3,1,1) %(fila, columna, pos)
plot(data_flex, 'r')
title('Flex')
hold on
plot(data_flex_act, 'k')
hold on
plot(vect_umbral_flex, 'g')
hold on
plot(data_pulsador*1000,'k')

subplot(3,1,2)
plot(data_ext_act, 'b')
title('Ext')
hold on
% plot(data_act*300, 'k')
% hold on
plot(vect_umbral_ext, 'g')
hold on
plot(data_pulsador*1000,'k')

subplot(3,1,3)
plot(data_modo_act, 'b')
title('Derivada')
hold on
plot(data_derivada, 'm')
hold on
plot(vect_derivada, 'g')
hold on
plot(data_pulsador*10,'k')

figure(2)
plot(data_flex_act, 'r')
hold on
plot(data_ext_act, 'b')
hold on
%plot((data_act*300)+500, 'k')
hold on
plot((data_pulsador*800), 'k')
