clear
close all
clc

%%
%% Lectura de datos del archivo excel
%%
dataEMG = xlsread('id07/Data_id07.xlsx', 'Hoja5');
data_flex = dataEMG(:,1);
data_ext = dataEMG(:,2);
data_derivada = dataEMG(:,3);
%data_act = dataEMG(:,4);
data_modo = dataEMG(:,4);
data_pulsador = dataEMG(:,5);

umbral_ext = dataEMG(1,6)*dataEMG(1,8);
umbral_flex = (dataEMG(1,7)*dataEMG(1,9));
umbral_Derivada = dataEMG(1,10);
frec_muestreo = dataEMG(1,11);

%%
%% Buscador de indices en modo activado
%%
%vect_index_ext = find(data_act == 1);
%vect_index_flex = find(data_act == 2);
vect_index_pulsador = find(data_pulsador == 1);

%%
%% Correcci�n de error de escritura
%%
for i=1:1:length(data_flex(:,1))
    if data_flex(i,1) > 5
        data_flex_act(i,1) = data_flex(i,1);
    else
        data_flex_act(i,1) = data_flex_act(i-1,1);
    end
end

for i=1:1:length(data_ext(:,1))
    if data_ext(i,1) > 5
        data_ext_act(i,1) = data_ext(i,1);
    else
        data_ext_act(i,1) = data_ext_act(i-1,1);
    end
end

%%
%% Valor de area bajo la curva del setup
%%
area_norm_flex=dataEMG(1,7)*5*frec_muestreo;
area_norm_ext=dataEMG(1,6)*5*frec_muestreo;

%%
%% Generaci�n de vector de integral
%%
min_data_flex=min(data_flex_act);
for i=1:1:length(data_flex_act)
    if data_flex_act(i,1) > umbral_flex
        flex_integral(i) = data_flex_act(i,1) - min_data_flex;
    else
        flex_integral(i) = 0;
    end
end
min_data_ext=min(data_ext_act);
for i=1:1:length(data_ext_act)
    if data_ext_act(i,1) > umbral_ext
        ext_integral(i) = data_ext_act(i,1) - min_data_ext;
    else
        ext_integral(i) = 0;
    end
end

%%
%% C�lculo de la integral
%%
for j=1:1:6
    esfuerzo_flex(j)=trapz(flex_integral(vect_index_pulsador(j):vect_index_pulsador(j+1)))/area_norm_flex;
    esfuerzo_ext(j)=trapz(ext_integral(vect_index_pulsador(j):vect_index_pulsador(j+1)))/(area_norm_ext);
end

for j=1:1:6
    esfuerzo_muscular(j)=esfuerzo_ext(j)+esfuerzo_flex(j)    
end

for j=1:1:6
    %cant_muestras(j)=length(data_flex(vect_index_pulsador(j):vect_index_pulsador(j+1)))    
end

% for j=1:1:(length(vect_index_pulsador)-1)
%     flex_integral=[];
%     esfuerzo_flex(j) = 0;
%     esfuerzo_ext(j) = 0;
%     for i=1:1:length(vect_index_flex)
%         if ((vect_index_flex(i,1) > vect_index_pulsador(j)) && (vect_index_flex(i,1) < vect_index_pulsador(j+1)))  
%             flex_integral = [flex_integral; data_flex_act(vect_index_flex(i))];
%         end
%     end
%     for i=1:1:length(vect_index_ext)
%         if ((vect_index_ext(i,1) > vect_index_pulsador(j)) && (vect_index_ext(i,1) < vect_index_pulsador(j+1)))  
%             ext_integral(i) = data_ext_act(vect_index_ext(i));
%         end
%     end    
%     esfuerzo_flex(j) = trapz(flex_integral)/umbral_flex;
%     esfuerzo_ext(j) = trapz(ext_integral)/umbral_ext;
%     
%     subplot(2,1,2) %(fila, columna, pos)
%     plot(flex_integral, 'r')
%     % pause
% end

%%
%% Plot de los datos necesarios
%%
figure(1)
subplot(2,1,1) %(fila, columna, pos)
plot(data_flex_act, 'r')
title('EMG - Flex')
hold on
% plot(flex_integral, 'g')
hold on
%plot(data_act*300, 'k')
hold on
plot(data_pulsador*1000,'k')

subplot(2,1,2) %(fila, columna, pos)
plot(data_ext_act, 'b')
title('EMG - Ext')
hold on
% plot(ext_integral, 'g')
hold on
%plot(data_act*300, 'k')
hold on
plot(data_pulsador*1000,'k')

% subplot(3,1,3) %(fila, columna, pos)
% plot(esfuerzo_flex, 'r')
% hold on
% plot(esfuerzo_ext, 'b')
% title('Esfuerzo Muscular')

% for j=1:1:(length(vect_index_pulsador)-1)
%     figure(2)
%     subplot(3,1,1) %(fila, columna, pos)
%     plot(flex_integral(vect_index_pulsador(j):vect_index_pulsador(j+1)), 'r')
%     title('Flex')
% 
%     subplot(3,1,2) %(fila, columna, pos)
%     plot(ext_integral(vect_index_pulsador(j):vect_index_pulsador(j+1)), 'b')
%     title('Ext')
% 
%     subplot(3,1,3) %(fila, columna, pos)
%     plot(esfuerzo_flex, 'r')
%     hold on
%     plot(esfuerzo_ext, 'b')
%     title('Esfuerzo Muscular')    
% end
 
% figure(3)
% plot(esfuerzo_flex, 'r')
% hold on
% plot(esfuerzo_ext, 'b')
% legend('Flexi�n','Extensi�n')
% title('Esfuerzo Muscular') 
    
    
    