//
//  Script de asociado a la esfera/pelota
//
//  Detecta el inicio (OnTriggerEnter) y el fin (OnTriggerExit) del contacto entre la esfera y cada uno de los dedos.
//
//  Polling en el flag de contacto de todos los dedos.
//
//  Una vez que la esfera tiene contacto con (todos?) los dedos deja de responder a la gravedad y 
//    comienza a resonder a los movimientos del teclado al igual que la mano hasta que la mano se abre.
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

using System;
using System.IO;

public class Object_Control : MonoBehaviour
{
  private new Rigidbody rigidbody;

  int cont_move_x=0;
  int cont_move_y=0;
  float speed = 0.3f;
  public static float time_frame = 0.0f;


  public static Vector3 vect_pos_obj;

  //
  //-------------------- START --------------------
  //

  // Start is called before the first frame update
  void Start()
  {
    rigidbody=GetComponent<Rigidbody>();
  }

  //
  //-------------------- UPDATE --------------------
  //

  // Update is called once per frame
  void Update()
  {
    // Debug.Log(GameManager.flag_contact_all);
    
    //__________________________________________________________________
    //Confirma el contacto del objeto con la mano
    //El objeto deja de responder a la gravedad y pasa a ser Kinematic
    //Comienza a responder a los mismos movimientos de la mano
    //
    //También hace el proceso inverso, confirma el fin del contacto 
    //El objeto vuelve a responder a la física
    //__________________________________________________________________
    if (GameManager.flag_contact_all == true)
    {
      //Debug.Log("<color=red> CONTACTO DE TODOS LOS DEDOS </color>");
      rigidbody.useGravity = false;
      rigidbody.isKinematic = true;
      //GameManager.flag_contact_all = 0;

      if ((Input.GetAxis("Horizontal") == 1) || (GameManager.com_horizontal == "1"))
      {
        if (cont_move_x<10000){
          rigidbody.MovePosition(rigidbody.position + Vector3.right * 0.5f*speed * Time.deltaTime);     
          //transform.Translate (1 * Time.deltaTime, 0, 0); //se mueve en el eje x     
          cont_move_x++;
          //Debug.Log(cont_move_x);
        }
      } 
      
      if ((Input.GetAxis("Horizontal") == -1) || (GameManager.com_horizontal == "-1"))
      {
        if (cont_move_x>-10000){
          rigidbody.MovePosition(rigidbody.position + Vector3.left * 0.5f*speed * Time.deltaTime);   
          //transform.Translate (-1 * Time.deltaTime, 0, 0); //se mueve en el eje x              
          cont_move_x--;
          //Debug.Log(cont_move_x);
        }
      }
              
      if ((Input.GetAxis("Vertical") == 1) || (GameManager.com_vertical == "1"))
      {
        if (cont_move_y<10000){
          rigidbody.MovePosition(rigidbody.position + Vector3.up * 0.5f*speed * Time.deltaTime);           
          //transform.Translate (0, 1 * Time.deltaTime, 0); //se mueve en el eje y                
          cont_move_y++;
          //Debug.Log(cont_move_y);
        }
      } 
      
      if ((Input.GetAxis("Vertical") == -1) || (GameManager.com_vertical == "-1"))
      {
        if (cont_move_y>-10000){
          rigidbody.MovePosition(rigidbody.position + Vector3.down * 0.5f*speed * Time.deltaTime);           
          //transform.Translate (0, -1 * Time.deltaTime, 0); //se mueve en el eje y                
          cont_move_y--;
          //Debug.Log(cont_move_y);
        }
      }
    }
    else if (GameManager.flag_contact_all == false){
      rigidbody.useGravity = true;
      rigidbody.isKinematic = false;
    }
    
    //___________________________________________________
    //Confirma el contacto de los dedos correspondientes
    // Index, Middle o Ring, al menos dos de estos más Thumb
    //___________________________________________________
    if ((GameManager.flag_mode == 0 || GameManager.flag_mode == 1) && GameManager.flag_contact_thumb == true && ((GameManager.flag_contact_index == true && GameManager.flag_contact_middle == true) || (GameManager.flag_contact_index == true && GameManager.flag_contact_ring == true) || (GameManager.flag_contact_middle == true && GameManager.flag_contact_ring == true)) || (GameManager.flag_mode == 2 && GameManager.flag_contact_thumb == true && GameManager.flag_contact_index == true))
    {
      Debug.Log("<color=red> CONTACTO DE LOS 5 DEDOS </color>");
      GameManager.flag_contact_all = true;
      GameManager.flag_contact_thumb = false;
      GameManager.flag_contact_index = false;
      GameManager.flag_contact_middle = false;
      GameManager.flag_contact_ring = false;
      GameManager.flag_contact_pinky = false;
    }

    //___________________________________________________________
    //Confirma el contacto del objeto con el "canasto"
    //Destruye el objeto y activa el flag para crear el siguiente
    //___________________________________________________________
    if(GameManager.flag_contact_objetivo == true){
      Debug.Log("DESTRUIR OBJETO");
      GameManager.flag_contact_objetivo = false;
      Destroy(gameObject, 0.02f);
      GameManager.flag_destroyObject = true;
      time_frame = Time.time - Game_Control.time_init; 
            
      if (GameManager.flag_contact_piso_inferior == true){
        Debug.Log(GameManager.flag_contact_piso_inferior);
        GameManager.flag_contact_piso_inferior = false;
        Debug.Log("EL OBJETO SE PERDIÓ");
        vect_pos_obj = rigidbody.position;
        GameManager.array_puntos[(Game_Control.value_spheres - 1), 2] = 0.0f;
      }
      else{
        GameManager.array_puntos[(Game_Control.value_spheres - 1), 2] = 1.0f;
        vect_pos_obj = new Vector3(0, 1.7f, 0.17f);
      }

      GameManager.flag_explosion = true;

      GameManager.array_puntos[(Game_Control.value_spheres - 1), 0] = time_frame;
      GameManager.array_puntos[(Game_Control.value_spheres - 1), 1] = GameManager.dist_objeto; 
      GameManager.array_puntos[(Game_Control.value_spheres - 1), 3] = Mathf.Round(((GameManager.dist_objeto/time_frame)*1000*GameManager.array_puntos[(Game_Control.value_spheres - 1), 2]*100) / 100);
    }
  }

  //
  //-------------------- COLLIDERS --------------------
  //

    //__________________________________________________________________
    //Detecta el INICIO del contacto de el objeto con los dedos 
    // y con el "canasto" (objetivo)
    //__________________________________________________________________
  void OnTriggerEnter(Collider collision)
  {
    if (collision.gameObject.tag == "Thumb")
    {
      //Debug.Log("CONTACTO THUMB");
      GameManager.flag_contact_thumb = true;
    }

    if (collision.gameObject.tag == "Index") 
    {
      //Debug.Log("CONTACTO INDEX");
      GameManager.flag_contact_index = true;
    }

    if (collision.gameObject.tag == "Middle") 
    {
      //Debug.Log("CONTACTO MIDDLE");
      GameManager.flag_contact_middle = true;
    }

    if (collision.gameObject.tag == "Ring")
    {
      //Debug.Log("CONTACTO RING");
      GameManager.flag_contact_ring = true;
    }

    if (collision.gameObject.tag == "Pinky") 
    {
      //Debug.Log("CONTACTO PINKY");
      GameManager.flag_contact_pinky = true;
    }

    if(collision.gameObject.tag == "objetivo")
    {
      //Debug.Log("Contacto canasto");
      GameManager.flag_contact_objetivo=true;
    }

    if(collision.gameObject.tag == "Piso_Inferior")
    {
      //Debug.Log("Contacto canasto");
      GameManager.flag_contact_objetivo = true;
      GameManager.flag_contact_piso_inferior = true;
    }
  }

  //__________________________________________________________________
  //Detecta el FINAL del contacto de el objeto con los dedos 
  //__________________________________________________________________
  void OnTriggerExit(Collider collision)
  {
    if (collision.gameObject.tag == "Thumb"){
      GameManager.flag_contact_thumb = false;
    }

    if (collision.gameObject.tag == "Index"){
      GameManager.flag_contact_index = false;
    }

    if (collision.gameObject.tag == "Middle"){
      GameManager.flag_contact_middle = false;
    }

    if (collision.gameObject.tag == "Ring"){
      GameManager.flag_contact_ring = false;
    }

    if (collision.gameObject.tag == "Pinky"){
      GameManager.flag_contact_pinky = false;
    }
  }


}