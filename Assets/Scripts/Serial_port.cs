using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO.Ports;


public class Serial_port : MonoBehaviour
{
    private string DataJSON;
    public SerialPort serialPort = new SerialPort("COM11", 9600);

    [System.Serializable]
    public class Data_Arduino
    {
        public string Val_x;
        public string Val_y;
        public string Val_EMG;
        public int Val_Modo;
        public int Val_Puls;
    }
    
    void Start()
    {
        serialPort.Open ();	//abre el puerto serial        
    }

    // Update is called once per frame
    void Update()
    {   
        //serialPort.Open ();	//abre el puerto serial
        if (!serialPort.IsOpen) {
            serialPort.Open ();	//abre el puerto serial        
        }
    
        DataJSON = serialPort.ReadLine ();
        Data_Arduino Data_rx = (Data_Arduino)JsonUtility.FromJson(DataJSON,  typeof(Data_Arduino));
        GameManager.com_horizontal = Data_rx.Val_x;
        GameManager.com_vertical = Data_rx.Val_y;
        GameManager.com_hand = Data_rx.Val_EMG;
        GameManager.com_modo = Data_rx.Val_Modo;
        GameManager.com_pulsador = Data_rx.Val_Puls;
        
        // Debug.Log(P.Val_x);
        // Debug.Log(P.Val_y);
        // Debug.Log(P.Val_EMG); 
                  
        serialPort.Close (); //cierra el puerto serial
    }
}
