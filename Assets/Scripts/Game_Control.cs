//
//  Script de control de tiempo, texto en pantalla
//      y creación de nuevos objetos
//
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

//using System;

public class Game_Control : MonoBehaviour
{
    public static int value_spheres = 0;
    public static float time_init = 0.0f;
    public static float time_act = 0.0f;
    public static int cont_antirebote = 0;
    public static float puntos_acumulativo = 0;
    public static int cont_spheres = 0;
    public static int cont_cylinder = 0;
    public static int cont_cube = 0;        
    public static bool flag_object = false;
    
    public Text text_time; //Indicador de tiempo actual
    public Text text_intentos;
    public Text text_fin;
    public Text text_puntos_pos;
    public Text text_puntos_neg;
    public Text text_puntos_acumulativo;
    public Text text_modo;
    public Text text_Sujetado;
    public Text text_NoSujetado;

    //
    //-------------------- START --------------------
    //
    // Start is called before the first frame update
    void Start()
    {
        text_puntos_acumulativo.text = "Puntos: " + puntos_acumulativo;
    }
    
    //
    //-------------------- UPDATE --------------------
    //
    // Update is called once per frame
    void Update()
    {
        //___________________________________________________________
        //Control de tiempos
        //___________________________________________________________
        if ((time_init != 0.0f) && GameManager.flag_destroyObject == false ) {
            time_act = Time.time - time_init;
        }
        if (time_act > GameManager.time_max){
            GameManager.flag_contact_piso_inferior = true;
            GameManager.flag_contact_objetivo = true;
        }
        time_act = Mathf.Round( time_act * 100f ) / 100f;

        //___________________________________________________________
        //Control de textos en pantalla
        //___________________________________________________________
        if ((GameManager.flag_destroyObject == true)){
            text_time.text = "Último tiempo: " + time_act + " segundos";
            cont_antirebote++;
            
            if (cont_antirebote <= 1){
                GameManager.flag_escritura = true;
            }
            
            if (GameManager.array_puntos[(Game_Control.value_spheres - 1), 3] != 0){
                text_puntos_acumulativo.text = "Puntos: " + puntos_acumulativo;
                text_puntos_pos.text = "+" + GameManager.array_puntos[(Game_Control.value_spheres - 1), 3];
            }
            else
            {
                text_puntos_neg.text = "0";
            }
        }
        else
        {
            text_time.text = "Tiempo actual: " + time_act + " segundos";
            cont_antirebote = 0;
            GameManager.flag_escritura = false;
            text_puntos_pos.text = "";
            text_puntos_neg.text = "";            
        }

        if (value_spheres <= GameManager.max_intentos){
            text_intentos.text = "Número de intentos: " + Game_Control.value_spheres;
        }
        else if (value_spheres == (GameManager.max_intentos + 1))
        {
            text_puntos_pos.text = " ";
            text_puntos_neg.text = " "; 
            text_time.text = " ";
            text_puntos_acumulativo.text = " ";
            text_intentos.text = " ";
        }

        if (GameManager.flag_mode == 0){
            text_modo.text = "Modo Fuerza";
        }
        else if (GameManager.flag_mode == 1){
            text_modo.text = "Modo Cilíndrico";
        }
        else if (GameManager.flag_mode == 2){
            text_modo.text = "Modo Pinza";
        }

        /*
        if (GameManager.flag_contact_all == true){
            text_Sujetado.text = " • ";
            text_NoSujetado.text = " ";
        }
        else if (GameManager.flag_contact_all == false){
            text_Sujetado.text = " "; 
            text_NoSujetado.text = " • ";
        }        
        */

        //___________________________________________________________
        //Control de creación de nuevos objetos
        //___________________________________________________________
        if ((Input.GetButtonDown("Enter")) || (GameManager.com_pulsador == 1)){
            if (Game_Control.value_spheres < GameManager.max_intentos){
                GameManager.flag_create = true;
                GameManager.com_pulsador = 0;
                do{
                    GameManager.flag_number_object = Random.Range(1, 4);
                    if ((GameManager.flag_number_object == 1) && (cont_spheres < 2)){
                        cont_spheres++;
                        flag_object = true;
                    }
                    else if ((GameManager.flag_number_object == 2) && (cont_cylinder < 2)){
                        cont_cylinder++;
                        flag_object = true;
                    } 
                    else if ((GameManager.flag_number_object == 3) && (cont_cube < 2)){
                        cont_cube++;
                        flag_object = true;
                    } 
                    else{
                        flag_object = false;
                    }                                        
                }while(flag_object == false);
                Debug.Log("CREAR UN NUEVO OBJETO");
                // puntos_acumulativo += GameManager.array_puntos[(Game_Control.value_spheres - 1), 3];
            }
            else if(Game_Control.value_spheres == GameManager.max_intentos){
                Debug.Log("Máximo de intentos");
                text_fin.text = "\n" + "GAME OVER" + "\n" + "\n" + "PUNTOS TOTALES: " + puntos_acumulativo; 
            }
            else if(Game_Control.value_spheres == (GameManager.max_intentos + 1)){
                EditorApplication.isPlaying = false; //Sale de la reproducción del juego
            }

            Game_Control.value_spheres++;
        }
    }
}
  
