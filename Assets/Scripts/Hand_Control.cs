//
//Script de control de movimiento de mano y eventualmente el object de interés
//Botones: Flechas del teclado
//          - Horizontal (+) = Flecha Derecha
//          - Horizontal (-) = Flecha Izquierda
//          - Vertical   (+) = Flecha Arriba
//          - Vertical   (-) = Flecha Abajo
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class Hand_Control : MonoBehaviour
{
    private new Rigidbody rigidbody;
    public static Vector3 vect_pos_hand;

    int cont_move_x=0;
    int cont_move_y=0;
    float speed = 0.3f;
    //public static float cont_aux = 0;

    //
    //-------------------- START --------------------
    //
    // Start is called before the first frame update
    void Start()
    {
        rigidbody=GetComponent<Rigidbody>();
        Debug.Log("Flag Mode");
        Debug.Log(GameManager.flag_mode); 
    }

    //
    //-------------------- UPDATE --------------------
    //
    void Update()
    {   
        vect_pos_hand = rigidbody.position;
        //Debug.Log(GameManager.dist_objeto);
        if ((Input.GetAxis("Horizontal") == 1) || (GameManager.com_horizontal == "1"))
        {
            if (cont_move_x<10000){
                rigidbody.MovePosition(rigidbody.position + Vector3.right * 0.5f*speed * Time.deltaTime);     
                //transform.Translate (1 * Time.deltaTime, 0, 0); //se mueve en el eje x     
                cont_move_x++;
                //Debug.Log(cont_move_x);
            }
        } 
        
        if ((Input.GetAxis("Horizontal") == -1) || (GameManager.com_horizontal == "-1"))
        {
            if (cont_move_x>-10000){
                rigidbody.MovePosition(rigidbody.position + Vector3.left * 0.5f*speed * Time.deltaTime);   
                //transform.Translate (-1 * Time.deltaTime, 0, 0); //se mueve en el eje x              
                cont_move_x--;
                //Debug.Log(cont_move_x);
            }
        }
                
        if ((Input.GetAxis("Vertical") == 1) || (GameManager.com_vertical == "1"))
        {
            if (cont_move_y<10000){
                rigidbody.MovePosition(rigidbody.position + Vector3.up * 0.5f*speed * Time.deltaTime);           
                //transform.Translate (0, 1 * Time.deltaTime, 0); //se mueve en el eje y                
                cont_move_y++;
                //Debug.Log(cont_move_y);
            }
        } 
        
        if ((Input.GetAxis("Vertical") == -1) || (GameManager.com_vertical == "-1"))
        {
            if (cont_move_y>-10000){
                rigidbody.MovePosition(rigidbody.position + Vector3.down * 0.5f*speed * Time.deltaTime);           
                //transform.Translate (0, -1 * Time.deltaTime, 0); //se mueve en el eje y                
                cont_move_y--;
                //Debug.Log(cont_move_y);
            }
        }
        
        //___________________________________________________
        //Cambio de modo de manejo de la mano
        //Flag_mode = 1 --> Modo fuerza (mano completa)
        //Flag_mode = 2 --> Modo agarre cilíndrico
        //Flag_mode = 3 --> Modo Pinza
        //___________________________________________________
        
        GameManager.flag_mode = GameManager.com_modo;
        
        /*
        if (Input.GetButtonDown("Jump"))
        {
            if (GameManager.flag_mode == 0 ){
                GameManager.flag_mode = 1;
            }
            else if (GameManager.flag_mode == 1){
                GameManager.flag_mode = 2;
                GameManager.flag_cambio_modo_12 = true;
                Debug.Log(GameManager.flag_cambio_modo_12);
            }
            else if (GameManager.flag_mode == 2){
                GameManager.flag_mode = 0;
            }
            Debug.Log("Flag Mode");
            Debug.Log(GameManager.flag_mode);            
        }
        */        
    }
}
