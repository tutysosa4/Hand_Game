﻿//
//SCRIPT AUXILIAR PARA INICIALIZAR VARIABLES AUXILIARES PARA CONECTAR SCRIPTS ENTRE ELLOS
//


public static class GameManager
{
    //___________________________________________________________
    //Flags de modo de manejo de mano
    // 0 - Modo Fuerza || 1 - Modo Cilindro || 2 - Modo Pinza
    //___________________________________________________________
	public static int flag_mode = 0;  
    
    //___________________________________________________________
    //Valores auxiliares para el paso de movimiento de la mano
    //___________________________________________________________
    public static float vel_step = 120.0f;
    public static float aux_step = 1.5f;
    public static float step_finger = 0.01f*aux_step;
    public static float step_finger_max = 10000.0f*1/(aux_step*aux_step);
    public static float aux_vel_rot = 2.0f;
    public static float aux_max_cont = 0.6f;
    //si el step finger se aumenta o disminuye por un factor de X, disminuir o aumentar este valor en X^2
      
    //___________________________________________________________
    //Flags de contacto
    //___________________________________________________________
    public static bool flag_contact_thumb = false;
    public static bool flag_contact_index = false;
    public static bool flag_contact_middle = false;
    public static bool flag_contact_ring = false;
    public static bool flag_contact_pinky = false;
    public static bool flag_contact_all = false;
    public static bool flag_contact_objetivo = false;
    public static bool flag_contact_piso_inferior = false;

    //___________________________________________________________
    //Flags de creación o destrucción de objetos
    //___________________________________________________________
    public static bool flag_create = false;
    public static int flag_number_object = 1;
    public static bool flag_destroyObject = false;
    public static bool flag_explosion = false;

    //___________________________________________________________
    //Variables auxiliares para puntos
    //___________________________________________________________
    public static bool flag_escritura = false;
    public static float dist_objeto = 0.0f; 
    public static float[,] array_puntos = new float[10, 4];
    public static float time_max = 120.0f; //Tiempo máximo en segundos
    public static int max_intentos = 6;

    //___________________________________________________________
    //Variables Auxiliares de comunicación serial
    //___________________________________________________________
    public static string com_horizontal;// = 0.0f;
    public static string com_vertical;
    public static string com_hand;
    public static int com_modo = 2; 
    public static int com_pulsador;
}
