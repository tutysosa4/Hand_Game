using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createExplosion : MonoBehaviour
{
    public GameObject explosion;
    public GameObject explosionAnt;
    GameObject[] find;
    Vector3 vect_pos_explosion;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.flag_explosion == true){
            explosionAnt = GameObject.FindWithTag("explosion"); //se fija si ya hay un objeto creado con ese tag
            if (explosionAnt != null)
            { //si hay un arquero arquero1 = 1
                DestroyImmediate(explosionAnt, true); //Destruye el arquero anterior
            }
            vect_pos_explosion = Object_Control.vect_pos_obj;
            Instantiate(explosion, vect_pos_explosion, Quaternion.identity);    
            explosion.tag="explosion";
            GameManager.flag_explosion = false;
        }
    }
}
