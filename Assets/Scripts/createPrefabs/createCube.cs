using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createCube : MonoBehaviour
{
    public GameObject cube;
    GameObject[] find;
    Vector3 vect_pos_cube;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.flag_create==true){
            GameManager.flag_contact_objetivo = false;
            GameManager.flag_contact_piso_inferior = false;

            if (GameManager.flag_number_object == 3){
                //GameManager.flag_contact_objetivo = false;
                find = GameObject.FindGameObjectsWithTag("Objeto");
                if (find.Length == 0){
                    do{
                        vect_pos_cube =  new Vector3(Random.Range(-0.6f, 0.6f), Random.Range(1.5f, 2.0f), 0.085f); 
                    }while ((vect_pos_cube[0] < 0.35f && vect_pos_cube[0] > -0.35f));
                    Instantiate(cube, vect_pos_cube, Quaternion.identity);
                    cube.tag="Objeto";                    
                    GameManager.flag_create=false;
                    GameManager.flag_destroyObject = false;

                    Game_Control.time_init = Time.time;
                    //Debug.Log(Game_Control.time_frame);

                    GameManager.dist_objeto = Mathf.Sqrt(Mathf.Pow((Hand_Control.vect_pos_hand[0] - vect_pos_cube[0]), 2) + Mathf.Pow((Hand_Control.vect_pos_hand[1] - vect_pos_cube[1]), 2));
                    //Debug.Log(Hand_Control.vect_pos_hand);
                    //Debug.Log(vect_pos_cube);
                    //Debug.Log(GameManager.dist_objeto);
                }
            }
        }        
    }
}
