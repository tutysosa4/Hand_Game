using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ring_02 : MonoBehaviour
{
    float val_ajuste_x=20.0f;
    float val_ajuste_y=-25.0f;
    float val_ajuste_z=0.0f;

    float max_value_x=55.0f;
    float max_value_y=80.0f;
    float max_value_z=75.0f;
  
    float init_value_x=0.0f;
    float init_value_y=30.0f;
    float init_value_z=0.0f;

    float val_rot_x=0.0f;
    float val_rot_y=0.0f;
    float val_rot_z=0.0f;

    float step=0.0f;
    float max_cont=0.0f;
    float cont_paso=0.0f;

    // Start is called before the first frame update
    void Start()
    {
        step=GameManager.step_finger;
        max_cont=GameManager.step_finger_max*step;
        
        max_value_x+=val_ajuste_x;
        max_value_y+=val_ajuste_y;
        max_value_z+=val_ajuste_z;

        val_rot_x=max_value_x-init_value_x;
        val_rot_y=max_value_y-init_value_y;
        val_rot_z=max_value_z-init_value_z;    
    }

    // Update is called once per frame
    void Update()
    {
        if ((GameManager.flag_mode == 0) || (GameManager.flag_mode == 1)) // Modo Fuerza o Modo Cilíndrico
        {
            if ((Input.GetAxis("Close_Open_Hand") == 1 ) || (GameManager.com_hand == "1")) {
                if (GameManager.flag_contact_ring == false){
                    if (cont_paso <= max_cont/(GameManager.aux_vel_rot)){
                        transform.Rotate((GameManager.aux_vel_rot * val_rot_x * step), (GameManager.aux_vel_rot * val_rot_y * step), (GameManager.aux_vel_rot * val_rot_z * step));
                        /*
                        Debug.Log("val_rot_x");
                        Debug.Log(val_rot_x);
                        */
                        cont_paso++;
                    }
                }
            } 
            
            if ((Input.GetAxis("Close_Open_Hand") == -1 ) || (GameManager.com_hand == "-1"))
            {
                if (cont_paso>0){
                    transform.Rotate(((-1) * GameManager.aux_vel_rot * val_rot_x * step), ((-1) * GameManager.aux_vel_rot * val_rot_y * step), ((-1) * GameManager.aux_vel_rot * val_rot_z * step));
                    /*
                    Debug.Log("val_rot_x");
                    Debug.Log(val_rot_x);
                    */
                    cont_paso--;
                }
            } 
        }  
    }
}
