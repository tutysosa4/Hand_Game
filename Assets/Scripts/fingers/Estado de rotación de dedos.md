# Estado de rotación de dedos

## ***Thumb***
### **Thumb_01**
Abierto -->(0, 180, 0)

Cerrado --> (10, 128, -30)
            
Valor de ajuste --> (0, 0, 0)


### **Thumb_02**
Abierto --> (0, -45, 0)

Cerrado --> (0, -25, -20)

Valor de ajuste --> (0, 0, 0)

### **thumb_03**
Abierto --> (0, 20, 0)

Cerrado --> (50, 0, -25)
            
Valor de ajuste --> (0, 0, 0)

## ***Index***
### **Index_01**
Abierto --> (10, 0, -5)

Cerrado --> (35, 0, 5)

Valor de ajuste --> (0, 0, 0)


### **Index_02**
Abierto --> (0, -12, 0)

Cerrado --> (100, -12, 0)

Valor de ajuste --> (0, 0, 0)

### **Index_03**
Abierto --> (15, 10, 10)

Cerrado --> (40, 40, 30)

Valor de ajuste --> (16.16 , -10.259, -14.676)

## ***Middle***
### **Middle_01**
Abierto -->(0, 180, 0)

Cerrado --> (0, 128, -30)

Valor de ajuste --> (0, 0, 0)


### **Middle_02**
Abierto --> (0, -45, 0)

Cerrado --> (0, -25, -20)

Valor de ajuste --> (0, 0, 0)

### **Middle_03**
Abierto --> (0, 20, 0)

Cerrado --> (50, 0, -25)

Valor de ajuste --> (0, 0, 0)

## ***Ring***
### **Ring_01**
Abierto -->(0, 30, 5)

Cerrado --> (40, 0, 5)

Valor de ajuste --> (0, 0, 0)


### **Ring_02**
Abierto --> (0, 30, 0)

Cerrado --> (55, 80, 75)
30, 100, 100

Valor de ajuste --> (0, 0, 0)

### **Ring_03**
Abierto --> (0, 45, 0)

Cerrado --> (18, 45, 70)
20, 30, 50

Valor de ajuste --> (0, 0, 0)

## ***Pinky***
### **Pinky_01**
Abierto -->(-5, 45, 15)

Cerrado --> (0, 128, -30)

Valor de ajuste --> (0, 0, 0)


### **Pinky_02**
Abierto --> (0, -45, 0)

Cerrado --> (0, -25, -20)

Valor de ajuste --> (0, 0, 0)

### **Pinky_03**
Abierto --> (0, 20, 0)

Cerrado --> (50, 0, -25)

Valor de ajuste --> (0, 0, 0)
