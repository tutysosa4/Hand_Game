using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thu_01 : MonoBehaviour
{
    float val_ajuste_x=0.0f;
    float val_ajuste_y=0.0f;
    float val_ajuste_z=0.0f;

    float max_value_x=10.0f;
    float max_value_y=128.0f;
    float max_value_z=-30.0f;
  
    float init_value_x=0.0f;
    float init_value_y=180.0f;
    float init_value_z=0.0f;
    
    float val_rot_x=0.0f;
    float val_rot_y=0.0f;
    float val_rot_z=0.0f;

    float step = 0.0f;
    float max_cont_1 = 0.0f;
    float max_cont_2 = 0.0f;
    float cont_paso = 0.0f;

    bool flag_stop = false;

    // Start is called before the first frame update
    void Start()
    {
        step=GameManager.step_finger;
        max_cont_1 = GameManager.step_finger_max * step;
        max_cont_2 = GameManager.aux_max_cont * max_cont_1;
        
        max_value_x+=val_ajuste_x;
        max_value_y+=val_ajuste_y;
        max_value_z+=val_ajuste_z;

        val_rot_x=max_value_x-init_value_x;
        val_rot_y=max_value_y-init_value_y;
        val_rot_z=max_value_z-init_value_z;    
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.flag_mode == 1) //Modo Cilíndrico
        {
            if ((cont_paso <= max_cont_1/(GameManager.aux_vel_rot))){
                transform.Rotate((GameManager.aux_vel_rot * val_rot_x * step), (GameManager.aux_vel_rot * val_rot_y * step), (GameManager.aux_vel_rot * val_rot_z * step));
                /*
                Debug.Log("val_rot_x");
                Debug.Log(val_rot_x);
                */
                cont_paso++;
                flag_stop = true;

            }
            else{
                // flag_stop = true;
            }
        }

        if ((GameManager.flag_mode == 2)){// && (Hand_Control.cont_aux <= max_cont_2/(GameManager.aux_vel_rot))){
            if ((cont_paso > 0 ) && (flag_stop == true)){
                transform.Rotate(((-1) * GameManager.aux_vel_rot * val_rot_x * step), ((-1) * GameManager.aux_vel_rot * val_rot_y * step), ((-1) * GameManager.aux_vel_rot * val_rot_z * step));
                /*
                Debug.Log("val_rot_x");
                Debug.Log(val_rot_x);
                */
                cont_paso--;
            }
            else{
                flag_stop = false;
            }
        }

        if ((GameManager.flag_mode == 0)) // Modo Fuerza
        {
            if ((Input.GetAxis("Close_Open_Hand") == 1 ) || (GameManager.com_hand == "1"))
            {
                if (GameManager.flag_contact_thumb == false){
                    if (cont_paso <= max_cont_1/(GameManager.aux_vel_rot)){
                        transform.Rotate((GameManager.aux_vel_rot * val_rot_x * step), (GameManager.aux_vel_rot * val_rot_y * step), (GameManager.aux_vel_rot * val_rot_z * step));
                        /*
                        Debug.Log("val_rot_x");
                        Debug.Log(val_rot_x);
                        */
                        cont_paso++;
                    }
                }
            } 
            
            if ((Input.GetAxis("Close_Open_Hand") == -1) || (GameManager.com_hand == "-1"))
            {
                if (cont_paso>0){
                    transform.Rotate(((-1) * GameManager.aux_vel_rot * val_rot_x * step), ((-1) * GameManager.aux_vel_rot * val_rot_y * step), ((-1) * GameManager.aux_vel_rot * val_rot_z * step));
                    /*
                    Debug.Log("val_rot_x");
                    Debug.Log(val_rot_x);
                    */
                    cont_paso--;
                }
            }   
        }
        else if (GameManager.flag_mode == 2) //Modo Pinza
        {
            if ((Input.GetAxis("Close_Open_Hand") == 1 ) || (GameManager.com_hand == "1"))
            {
                if (GameManager.flag_contact_thumb == false){
                    if (cont_paso <= max_cont_2/(GameManager.aux_vel_rot)){
                        transform.Rotate((GameManager.aux_vel_rot * val_rot_x * step), (GameManager.aux_vel_rot * val_rot_y * step), (GameManager.aux_vel_rot * val_rot_z * step));
                        /*
                        Debug.Log("val_rot_x");
                        Debug.Log(val_rot_x);
                        */
                        cont_paso++;
                    }
                }
            } 
            
            if ((Input.GetAxis("Close_Open_Hand") == -1) || (GameManager.com_hand == "-1"))
            {
                if (cont_paso>0){
                    transform.Rotate(((-1) * GameManager.aux_vel_rot * val_rot_x * step), ((-1) * GameManager.aux_vel_rot * val_rot_y * step), ((-1) * GameManager.aux_vel_rot * val_rot_z * step));
                    /*
                    Debug.Log("val_rot_x");
                    Debug.Log(val_rot_x);
                    */
                    cont_paso--;
                }
            } 
        }
    }  
}


