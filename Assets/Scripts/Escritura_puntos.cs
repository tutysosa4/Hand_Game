using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class Escritura_puntos : MonoBehaviour
{
    // int cont_antirebote = 0;
    string path;

    // Start is called before the first frame update
    void Start()
    {
        path = @"D:\UNRC\GAV\Proyectos\Hand_Game\Game\Hand_Game\Test\puntos_202205013_id08.xls";
        // ESCTIBIR EN EL ARCHIVO
        if (!File.Exists(path))
        {
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(path));
        }
        using (StreamWriter sw = File.AppendText(path))
        {
            sw.WriteLine("Tiempo" + "\t" + "Distancia" + "\t" + "Exito" + "\t" + "Puntos" + "\t" + "Modo de Funcionamiento" + "\t" + "Objeto Sujetado");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.flag_escritura == true){
            using (StreamWriter sw = File.AppendText(path))
            {
                string str_time = GameManager.array_puntos[(Game_Control.value_spheres - 1), 0].ToString();
                string str_distancia = GameManager.array_puntos[(Game_Control.value_spheres - 1), 1].ToString();
                string str_exito = GameManager.array_puntos[(Game_Control.value_spheres - 1), 2].ToString();
                string str_puntos =  GameManager.array_puntos[(Game_Control.value_spheres - 1), 3].ToString();
                string str_modo = GameManager.flag_mode.ToString();
                string str_object = (GameManager.flag_number_object - 1).ToString();
                string lines = str_time + "\t" + str_distancia + "\t" + str_exito + "\t" + str_puntos + "\t" + str_modo + "\t" + str_object;
                sw.WriteLine(lines);    
                GameManager.flag_escritura = false;  
                Game_Control.puntos_acumulativo += (float)GameManager.array_puntos[(Game_Control.value_spheres - 1), 3];             
                // sw.WriteLine(GameManager.array_puntos[(Control_modo.value_spheres - 1), 0]);
                // sw.WriteLine(GameManager.array_puntos[(Control_modo.value_spheres - 1), 1]);
                // sw.WriteLine(GameManager.array_puntos[(Control_modo.value_spheres - 1), 2]);
            }               
        }    
    }
}
