#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <SD.h>

#define UMBRAL_EXT  0.7
#define UMBRAL_FLEX 0.6

#define GAIN_EXT 1
#define GAIN_FLEX 1

int i=0;

int interruptor_modo = 0; // Modo de funcionamiento de la mano

// Pines de Salida
int ledv=10; //Led Ext
int ledr=9; //Led Flex
int leda=8; //Led Derivada
int interruptor_led = LOW; // Acción Led derivada

// Pines de Entrada
int myoware1 = A0; //Pin Ext
int myoware2 = A1; //Pin Flex
int joystick1 = A2; //Pin Joystick X
int joystick2 = A3; // Pin Joystick Y

// Pines y Variables para SD
const int Pin_SD = 7;
String nombreArchivo = "DataEMG1.xls";
String dataString = "";
int flag_activacion = 0;
File dataFile;
bool flag_escritura = false;

// Variables para setup contracción maxima
float ext=0;
float flex=0;
float nivelExt=0;
float nivelFlex=0;
unsigned long int Emax=0;
unsigned long int Fmax=0;
unsigned long int suma1=0;
unsigned long int suma2=0;

// Variables auxiliares para calcular la derivada
float derivada=0;
float derivada_ant=0;
float derivada_ant_2=0;
float aux_derivada=0;
float derivadamax[3];
float umbralDerivada=0;
unsigned long t0=0;
unsigned long ta=0;
float ext0=0;
float flex0=0;
int V1[150];
int V2[150];
int V3[150];
unsigned long int suma3=0;
bool flag_posible = false;
int cont_derivadas = 0;

// Variables auxiliares de Joystick
float joy_x = 0;
float joy_y = 0;
float sal_x = 0;
float sal_y = 0;

// Variables auxiliares de Señal EMG
int salida_EMG = 0;
int salidaExt = 0;
int salidaFlex = 0;

// Variables para utilizar el pulsador
int pin_pulsador = 4;
int estado_pulsador = LOW;
int sal_pulsador = 0;


// Declaración mensaje JSON
String mensaje_json;
StaticJsonDocument<60> doc;


void setup() {  
  //
  // Valores Máximos de Flexión y Extensión determinados en Setup
  // Valores para utilizar el joystick --> Emax: 1023; Fmax:1023; umbralDerivada: 9;
  //
  Emax = 1023;
  Fmax = 1023;
  umbralDerivada = 9;
  
  derivada_ant = umbralDerivada;
  derivada_ant_2 = umbralDerivada;
  
  Serial.begin (9600);

  pinMode(ledv,OUTPUT);
  pinMode(ledr,OUTPUT);
  pinMode(leda,OUTPUT);

  pinMode(pin_pulsador, INPUT);

   // Inicializar tarjeta SD
  Serial.print(F("Initializing SD card..."));
  // see if the card is present and can be initialized:
  if (!SD.begin(Pin_SD)) {
    Serial.println(F("Card failed, or not present"));
    // don't do anything more:
    return;
  }
  Serial.println(F("SD card initialized."));
  delay(500);
  intializeFile(nombreArchivo);


  Serial.println(F("----- EMG Game Control -----\n"));
  delay(2000);
}


void loop() 
{  
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------- FUNCIONAMIENTO GENERAL ---------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // -------------------------------------------------------------------
  // -------------------- Lectura de valores de EMG --------------------
  // -------------------------------------------------------------------
  ext = GAIN_EXT * (analogRead(myoware1));
  flex = GAIN_FLEX * (analogRead(myoware2));
  nivelExt = ext/Emax;
  nivelFlex = flex/Fmax;

  /*
  Serial.print(nivelExt);
  Serial.print("\t");
  Serial.println(nivelFlex);
  */
  
  // ----------------------------------------------------------------
  // -------------------- Cálculo de la derivada --------------------
  // ----------------------------------------------------------------
  ta = millis();
  derivada = (flex-flex0)/(ta-t0);
  flex0 = flex; //Valores usados para calcular la derivada
  t0 = ta; //Valores usados para calcular la derivada

  // --------------------------------------------------------------------
  // -------------------- Determinación del comando --------------------- 
  // --------------------------------------------------------------------
    /*
    Serial.print((derivada));
    Serial.print("\t");
    Serial.print((derivada > umbralDerivada));
    Serial.print("\t");
    Serial.println(flag_posible);
    delay(10);
    */
  // Serial.println((derivada));

  if ((derivada > umbralDerivada) || (flag_posible == true)) // Switch Modo de  Funcionamiento //
  { 
    flag_posible = true;
    cont_derivadas++;
    // Serial.println(derivada_ant_2 - derivada);
    if (((derivada_ant_2 - derivada) > (umbralDerivada*1.1)))
    {
      flag_posible = false;
      digitalWrite (ledr,LOW);
      digitalWrite (ledv,LOW);
      interruptor_led=!interruptor_led;
      digitalWrite (leda, interruptor_led);
      flag_activacion = 3;
      if (interruptor_modo < 2){
        interruptor_modo++;
      }
      else if (interruptor_modo == 2){
        interruptor_modo = 0; 
      }
      delay(200);
      derivada_ant = umbralDerivada;
      derivada_ant_2 = umbralDerivada;
      // Serial.print(F("Interruptor: "));    
    }  
    //delay(500);
    else{
      derivada_ant_2 = derivada_ant;
      derivada_ant = derivada;
    }

    if(cont_derivadas >= 3){
      cont_derivadas = 0;
      flag_posible = false;
      derivada_ant = umbralDerivada;
      derivada_ant_2 = umbralDerivada;
    }
    ta = millis(); 
  }     
  else if (nivelExt > UMBRAL_EXT) // Extension (salida = -1) //
  {     
    salidaExt++;          
    if(salidaExt >= 3) // Se envia Flexion cuando (nivelExt > UMBRAL_EXTS) en 2 loops consecutivos.
    {  
      // Serial.print(F("Extension"));
      salida_EMG = -1;
      digitalWrite (ledv,HIGH);  
      digitalWrite (ledr,LOW);
    }
    flag_activacion = 1;
  }           
  else if((nivelFlex > UMBRAL_FLEX) && (flag_posible == false)) // Flexion (salida = 1) //
  {
    salidaFlex++;
    if(salidaFlex >= 3) // Se envia Flexion cuando (nivelFlex > UMBRAL_FLEX) en 2 loops consecutivos.
    {  
      // Serial.print(F("Flexion"));
      salida_EMG = 1;
      digitalWrite (ledr,HIGH);  
      digitalWrite (ledv,LOW);
    }
    flag_activacion = 2;
  }
  else // En caso de no tener actividad, mantengo los motores apagados //
  {  
    //Serial.print(F("Reposo"));
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    salidaExt = 0;
    salidaFlex = 0;
    salida_EMG = 0;
    flag_activacion = 0;
  }

  // ------------------------------------------------------------------------
  // -------------------- Lectura de valores de joystick --------------------
  // ------------------------------------------------------------------------
  joy_x = analogRead(joystick1);
  joy_y= analogRead(joystick2);

  if (joy_x > 800 && joy_x < 1050){
    sal_x = 1;
  }
  else if (joy_x >= 0 && joy_x < 300){
    sal_x = -1;
  }
  else{
    sal_x = 0;
  }

  if (joy_y > 800 && joy_y < 1050){
    sal_y = 1;
  }
  else if (joy_y >= 0 && joy_y < 300){
    sal_y = -1;
  }
  else{
    sal_y = 0;
  }
  
  /*
  Serial.print("\t");
  Serial.print(sal_x);
  Serial.print("\t");
  Serial.println(sal_y);
  */

  // ------------------------------------------------------------------------
  // ------------------------ Lectura del pulsador --------------------------
  // ------------------------------------------------------------------------
  
  estado_pulsador = digitalRead(pin_pulsador);
  if (estado_pulsador == HIGH) {
    delay(50);
    estado_pulsador = digitalRead(pin_pulsador);
    if (estado_pulsador == HIGH){
      sal_pulsador = 1;
      delay(200);
    }
  }
  else
  {
    sal_pulsador = 0;
  }
  
  
  // ------------------------------------------------------------------------
  // ---------------------- Creación del paquete JSON -----------------------
  // ------------------------------------------------------------------------
  String mensaje_json;
  StaticJsonDocument<60> doc;
  doc["Val_x"] = sal_x;
  doc["Val_y"] = sal_y;
  doc["Val_EMG"] = salida_EMG;
  doc["Val_Modo"] = interruptor_modo;
  doc["Val_Puls"] = sal_pulsador;
  serializeJson(doc, mensaje_json);
  delay(10);

  // ----------------------------------------------------------------------------------------
  // --------------------------------- Grabar en memoria SD ---------------------------------
  // ----------------------------------------------------------------------------------------
  dataString = "";
  dataString = String(flex) + "\t" + String(ext) + "\t" + String(derivada) + "\t" + String(interruptor_modo) + "\t" + String(sal_pulsador); //preparar datos
  saveData(dataString,nombreArchivo);
  // Serial.print(dataString);
  delay(10);
  // dataString = "";
  // dataString = String(flag_activacion) + "\t" + String(interruptor_modo) + "\t" + String(sal_pulsador) + "\n"; //preparar datos
  // saveData(dataString,nombreArchivo);
  // Serial.print(dataString);

  
  // ----------------------------------------------------------------------------------------  
  // ---------------------- Envío del mensaje por comunicación serial -----------------------
  // ----------------------------------------------------------------------------------------
  if (flag_escritura == true){
    Serial.println(mensaje_json);
    delay(10);
    flag_escritura = false;
  }
}

// Función que inicializa un nuevo archivo
void intializeFile (String fileName)
{
  File dataFile = SD.open(fileName, FILE_WRITE); //Abrir archivo
  if (dataFile) 
  {
    Serial.println(F("Inicializando archivo..."));// inicializar
    
    dataFile.println("-------------------------------------");
    dataFile.println("Data EMG");
    dataFile.println("-------------------------------------");

    dataFile.println(String("Ext Max") + "\t" + String(Emax));
    dataFile.println(String("Flex Max") + "\t" + String(Fmax));
    dataFile.println(String("Umbral Ext") + "\t" + String(UMBRAL_EXT));
    dataFile.println(String("Umbral Flex") + "\t" + String(UMBRAL_FLEX));
    dataFile.println(String("Umbral Derivada") + "\t" + String(umbralDerivada));
    dataFile.print(String("Flexion") + "\t" + String("Extension") + "\t" + String("Derivada") + "\t");
    dataFile.println(String("Modo") + "\t" + String("Pulsador"));
    // dataFile.println(String("Flag Activacion") + "\t" + String("Modo") + "\t" + String("Pulsador"));
    dataFile.close();
    Serial.println(F("Archivo inicializado."));// inicializar
  }
  else // if the file isn't open, pop up an error:
  { 
    Serial.print(F("Error initializing "));
    Serial.println(fileName);
  }
  delay(200);
}

// Funcion que graba los datos en la memoria SD
void saveData(String dataString, String fileName)
{
  File dataFile = SD.open(fileName,(O_APPEND | O_WRITE));// (O_APPEND | O_WRITE)); //Abrir archivo
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    flag_escritura = true;

    // Serial.print(dataString);// print to the serial port too:
  }
  else // if the file isn't open, pop up an error:
  { 
     //Serial.print(F("error saving to "));
     //Serial.println(fileName);
     flag_escritura = false;
  }
}
  
