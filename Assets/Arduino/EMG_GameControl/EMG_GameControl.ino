#include <ArduinoJson.hpp>
#include <ArduinoJson.h>

#define UMBRAL_EXT  0.75
#define UMBRAL_FLEX 0.6

#define GAIN_EXT 2
#define GAIN_FLEX 3

int i=0;

int interruptor_modo = 0; // Modo de funcionamiento de la mano

// Pines de Salida
int ledv=10; //Led Ext
int ledr=9; //Led Flex
int leda=8; //Led Derivada
int interruptor_led = LOW; // Acción Led derivada

// Pines de Entrada
int myoware1 = A0; //Pin Ext
int myoware2 = A1; //Pin Flex
int joystick1 = A2; //Pin Joystick X
int joystick2 = A3; // Pin Joystick Y

// Variables para setup contracción maxima
float ext=0;
float flex=0;
float nivelExt=0;
float nivelFlex=0;

unsigned long int Emax=526;
unsigned long int Fmax=588;

unsigned long int suma1=0;
unsigned long int suma2=0;

// Variables auxiliares para calcular la derivada
float derivada=0;
float aux_derivada=0;
float derivadamax[3];
float umbralDerivada=0;
unsigned long t0=0;
unsigned long ta=0;
float ext0=0;
float flex0=0;
int V1[150];
int V2[150];
int V3[150];
unsigned long int suma3=0;

// Variables auxiliares de Joystick
float joy_x = 0;
float joy_y = 0;
float sal_x = 0;
float sal_y = 0;

// Variables auxiliares de Señal EMG
int salida_EMG = 0;
int salidaExt = 0;
int salidaFlex = 0;

// Declaración mensaje JSON
String mensaje_json;
StaticJsonDocument<50> doc;

void setup() {  
  Serial.begin (9600);

  pinMode(ledv,OUTPUT);
  pinMode(ledr,OUTPUT);
  pinMode(leda,OUTPUT);

  Serial.println(F("----- EMG Game Control -----\n"));
  delay(2000);

  //
  // Valores Máximos de Flexión y Extensión determinados en Setup
  //
  Emax = 499;
  Fmax = 633;
  umbralDerivada = 20;
}


void loop() 
{  
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------- FUNCIONAMIENTO GENERAL ---------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // -------------------------------------------------------------------
  // -------------------- Lectura de valores de EMG --------------------
  // -------------------------------------------------------------------
  ext = GAIN_EXT * analogRead(myoware1);
  flex = GAIN_FLEX * analogRead(myoware2);
  nivelExt = ext/Emax;
  nivelFlex = flex/Fmax;

  /*
  Serial.print(nivelExt);
  Serial.print("\t");
  Serial.println(nivelFlex);
  */
  
  // ----------------------------------------------------------------
  // -------------------- Cálculo de la derivada --------------------
  // ----------------------------------------------------------------
  ta = millis();
  derivada = (flex-flex0)/(ta-t0);

  // --------------------------------------------------------------------
  // -------------------- Determinación del comando --------------------- 
  // --------------------------------------------------------------------
  if (derivada > umbralDerivada) // Switch Modo de  Funcionamiento //
  { 
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    interruptor_led=!interruptor_led;
    digitalWrite (leda, interruptor_led);
    
    if (interruptor_modo < 2){
      interruptor_modo++;
    }
    else if (interruptor_modo == 2){
      interruptor_modo = 0; 
    }
    // Serial.print(F("Interruptor: "));
    
    delay(500);
    ta = millis();
  }     
  else if (nivelExt > UMBRAL_EXT) // Extension (salida = -1) //
  {     
    salidaExt++;          
    if(salidaExt >= 3) // Se envia Flexion cuando (nivelExt > UMBRAL_EXTS) en 2 loops consecutivos.
    {  
      // Serial.print(F("Extension"));
      salida_EMG = -1;
      digitalWrite (ledv,HIGH);  
      digitalWrite (ledr,LOW);
    }
  }           
  else if(nivelFlex > UMBRAL_FLEX) // Flexion (salida = 1) //
  {
    salidaFlex++;
    if(salidaFlex >= 3) // Se envia Flexion cuando (nivelFlex > UMBRAL_FLEX) en 2 loops consecutivos.
    {  
      // Serial.print(F("Flexion"));
      salida_EMG = 1;
      digitalWrite (ledr,HIGH);  
      digitalWrite (ledv,LOW);
    }
  }
  else // En caso de no tener actividad, mantengo los motores apagados //
  {  
    //Serial.print(F("Reposo"));
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    salidaExt = 0;
    salidaFlex = 0;
    salida_EMG = 0;
  }

  flex0 = flex; //Valores usados para calcular la derivada
  t0 = ta; //Valores usados para calcular la derivada

  // ------------------------------------------------------------------------
  // -------------------- Lectura de valores de joystick --------------------
  // ------------------------------------------------------------------------
  joy_x = analogRead(joystick1);
  joy_y= analogRead(joystick2);

  if (joy_x > 800 && joy_x < 1050){
    sal_x = 1;
  }
  else if (joy_x >= 0 && joy_x < 300){
    sal_x = -1;
  }
  else{
    sal_x = 0;
  }

  if (joy_y > 800 && joy_y < 1050){
    sal_y = 1;
  }
  else if (joy_y >= 0 && joy_y < 300){
    sal_y = -1;
  }
  else{
    sal_y = 0;
  }
  
  /*
  Serial.print("\t");
  Serial.print(sal_x);
  Serial.print("\t");
  Serial.println(sal_y);
  */

  // ------------------------------------------------------------------------
  // ---------------------- Creación del paquete JSON -----------------------
  // ------------------------------------------------------------------------
  String mensaje_json;
  StaticJsonDocument<50> doc;
  doc["Val_x"] = sal_x;
  doc["Val_y"] = sal_y;
  doc["Val_EMG"] = salida_EMG;
  doc["Val_Modo"] = interruptor_modo;
  serializeJson(doc, mensaje_json);
  delay(10);

  // ----------------------------------------------------------------------------------------
  // ---------------------- Envío del mensaje por comunicación serial -----------------------
  // ----------------------------------------------------------------------------------------
  Serial.println(mensaje_json);
  delay(10);      
}
