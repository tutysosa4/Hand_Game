#define GAIN_EXT 1
#define GAIN_FLEX 1

float joy = 0;
float salida = 0; 
float ext = 0;
float flex = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {

  joy= analogRead(A0);

  if (joy<500){
    salida = map(joy, 0, 500, 1023, 0);
    ext=salida;
    flex=0;
    
  }
  else if (joy>550){
    salida = map(joy, 500, 1023, 0, 1023);
    ext=0;
    flex=salida;
  }
  else if(joy > 500 && joy < 550){
    ext=0;
    flex=0;
  }

  Serial.print(ext);
  Serial.print(" ");
  Serial.println(flex);  
  
  //Serial.println(joy);  

}
