//#include <Arduino_JSON.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
    String json;
    StaticJsonDocument<300> doc;
    doc["text"] = "myText";
    doc["id"] = 10;
    doc["status"] = true;
    doc["value"] = 3.14;
    serializeJson(doc, json);
    Serial.println(json);
    delay(1000);

}
