#include <ArduinoJson.hpp>
#include <ArduinoJson.h>

float joy_x = 0;
float joy_y = 0;
float sal_x = 0;
float sal_y = 0;

String mensaje_json;
StaticJsonDocument<30> doc;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  joy_x= analogRead(A0);
  joy_y= analogRead(A1);


  if (joy_x > 800 && joy_x < 1050){
    sal_x = 1;
  }
  else if (joy_x >= 0 && joy_x < 300){
    sal_x = -1;
  }
  else{
    sal_x = 0;
  }

  if (joy_y > 800 && joy_y < 1050){
    sal_y = 1;
  }
  else if (joy_y >= 0 && joy_y < 300){
    sal_y = -1;
  }
  else{
    sal_y = 0;
  }
  
  String mensaje_json;
  StaticJsonDocument<30> doc;
  doc["Val_x"] = sal_x;
  doc["Val_y"] = sal_y;
  doc["Val_EMG"] = 0;
  serializeJson(doc, mensaje_json);
  delay(10);
  Serial.println(mensaje_json);
  delay(10);

}
