#define UMBRAL_EXT  0.7
#define UMBRAL_FLEX 0.6

#define GAIN_EXT 1
#define GAIN_FLEX 1

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//variables para contracción maxima
float myoware1=A0; //EXT. Derecho
float myoware2=A1; //FLEX. Izquierdo
float ext=0;
float flex=0;
float nivelExt=0;
float nivelFlex=0;
unsigned long int Emax=0;
unsigned long int Fmax=0;


//variables para derivada
int interruptor=LOW;
float ext0=0;
float flex0=0;
unsigned long t0=1;
unsigned long ta=0;
float derivada=0;
float umbralDerivada=0;


int ledv=10; //EXT
int ledr=9; //FLEX 
int leda=8; // SWITCH

int sendToUnity; //Variable de salida utilizada en el juego


void setup() {
  // put your setup code here, to run once:
  Serial.begin (9600);
  pinMode(ledv,OUTPUT); //Ext
  pinMode(ledr,OUTPUT); //Flex
  pinMode(leda,OUTPUT); //Switch
  umbralDerivada = 6;
  Emax = 1020;
  Fmax = 1020;
}

void loop() {
  // put your main code here, to run repeatedly:
  ext=GAIN_EXT*analogRead(myoware1);
  flex=GAIN_FLEX*analogRead(myoware2);
  nivelExt=ext/Emax;
  nivelFlex=flex/Fmax;
  
  ta = millis();
  derivada = (flex-flex0)/(ta-t0);
  //Serial.println(derivada);
  
  if (derivada > umbralDerivada) {
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    
    interruptor=!interruptor;
    digitalWrite (leda,interruptor);
    
    sendToUnity = 100;  

    delay(200);
  }     
  
  else if ( nivelExt > UMBRAL_EXT ) // fingerspread (mano abierta) //
  {          
    digitalWrite (ledv,HIGH);  
    digitalWrite (ledr,LOW);          
    sendToUnity = ((map(nivelExt*10, UMBRAL_EXT*10, 10, 10, 40)*0.1));
    //Mapea desde el umbral hasta 1 y la salida va desde 1 a 4
  }
              
  else if( nivelFlex > UMBRAL_FLEX) // fist (puño cerrado) //
  {    
    digitalWrite (ledr,HIGH);  
    digitalWrite (ledv,LOW);          
    sendToUnity = map(nivelFlex*10, UMBRAL_FLEX*10, 10, 10, 50)*(-0.1);
    //Mapea desde el umbral hasta 1 y la salida va desde -1 a -5.
  }
   
  else // En caso de no tener actividad, mantengo los motores apagados //
  {                                              
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    sendToUnity = 0;
  }
  
  flex0=flex;
  t0=ta;
  //Hace el cambio de variables para poder volver a calcular la derivada en la proxima vuelta

//  Serial.print(derivada);
//  Serial.print("\t");
  Serial.println(sendToUnity);
  delay(99);
}
