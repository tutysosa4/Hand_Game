#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <SD.h>


#define UMBRAL_EXT  0.75
#define UMBRAL_FLEX 0.6

#define GAIN_EXT 2
#define GAIN_FLEX 3

int i=0;

int interruptor_modo = 0; // Modo de funcionamiento de la mano

// Pines de Salida
int ledv=10; //Led Ext
int ledr=9; //Led Flex
int leda=8; //Led Derivada
int interruptor_led = LOW; // Acción Led derivada

// Pines de Entrada
int myoware1 = A0; //Pin Ext
int myoware2 = A1; //Pin Flex
int joystick1 = A2; //Pin Joystick X
int joystick2 = A3; // Pin Joystick Y

// Pines y Variables para SD
const int Pin_SD = 4;
String nombreArchivo = "Data_EMG.txt";
String dataString = "";

// Variables para setup contracción maxima
float ext=0;
float flex=0;
float nivelExt=0;
float nivelFlex=0;

unsigned long int Emax=526;
unsigned long int Fmax=588;

unsigned long int suma1=0;
unsigned long int suma2=0;

// Variables auxiliares para calcular la derivada
float derivada=0;
float aux_derivada=0;
float derivadamax[3];
float umbralDerivada=0;
unsigned long t0=0;
unsigned long ta=0;
float ext0=0;
float flex0=0;
int V1[150];
int V2[150];
int V3[150];
unsigned long int suma3=0;

// Variables auxiliares de Joystick
float joy_x = 0;
float joy_y = 0;
float sal_x = 0;
float sal_y = 0;

// Variables auxiliares de Señal EMG
int salida_EMG = 0;
int salidaExt = 0;
int salidaFlex = 0;

// Declaración mensaje JSON
String mensaje_json;
StaticJsonDocument<50> doc;


void setup() {  
  //
  // Valores Máximos de Flexión y Extensión determinados en Setup
  //
  Emax = 499;
  Fmax = 633;
  umbralDerivada = 20;
  
  Serial.begin (9600);

  pinMode(ledv,OUTPUT);
  pinMode(ledr,OUTPUT);
  pinMode(leda,OUTPUT);

   // Inicializar tarjeta SD
  Serial.print(F("Initializing SD card..."));
  // see if the card is present and can be initialized:
  if (!SD.begin(Pin_SD)) {
    Serial.println(F("Card failed, or not present"));
    // don't do anything more:
    return;
  }
  Serial.println(F("SD card initialized."));

  intializeFile(nombreArchivo);


  Serial.println(F("----- EMG Game Control -----\n"));
  delay(2000);

    // ---------------------------------------------------------------------------------------------------------------------------------------------------
  // ----------------------------------------------------------- SETUP CONTRACCIONES MÁXIMAS -----------------------------------------------------------
  // ---------------------------------------------------------------------------------------------------------------------------------------------------

  //
  // ------------------------------------------------------------------- EXTENSIÓN ---------------------------------------------------------------------
  //
  // En esta estructura se toman 150 muestras de la señal de EXTENSION durante una contracción sostenida, se calcula una media y se define la contracción máxima //
  // La extensión debe ser la contracción utilizada para abrir la mano y debe estar conectada al pin A0 //
  
  //
  // Al tercer encendido del led verde el usuario debe hacer la EXTENSIÓN
  //
  Serial.println(F("Mire las luz VERDE. Al tercer encendido hacer la extensión y mantener hasta que se apague la luz"));
  delay(4000);
  digitalWrite(ledv,HIGH);
  delay(500);
  digitalWrite(ledv,LOW);
  delay(500);
  digitalWrite(ledv,HIGH);
  delay(500);
  digitalWrite(ledv,LOW);
  delay(500);
  digitalWrite(ledv,HIGH);
  delay(500);

  while (i<500)
  {    
    suma1 = suma1 + (GAIN_EXT * analogRead(myoware1)); 
    i++;
    delay(10);
  }
  digitalWrite(ledv,LOW);
  
  //
  // ---------- Determinacion extensión máxima ----------
  //
  Emax=(suma1)/500;
  
  //
  // ---------- Reset de variables auxiliares ----------
  //
  i=0;
  suma1=0;

  //
  // -------------------------------------------------------------------- FLEXIÓN --------------------------------------------------------------------
  //
  // En esta estructura se toman 150 muestras de la señal de FLEXION durante una contracción sostenida, se calcula una media y se define la contracción máxima //
  // La flexión deber ser la contracción utilizada para abrir la mano y debe estar conectada al pin A1 //

  //
  // Al tercer encendido del led rojo el usuario debe hacer la FLEXIÓN
  //
  Serial.println(F("Mire las luz ROJA. Al tercer encendido hacer la flexión y mantener hasta que se apague la luz"));
  delay(4000);
  digitalWrite(ledr,HIGH);
  delay(500);
  digitalWrite(ledr,LOW);
  delay(500);
  digitalWrite(ledr,HIGH);
  delay(500);
  digitalWrite(ledr,LOW);
  delay(500);
  digitalWrite(ledr,HIGH);
  delay(500);
  
  while (i<500)
  {
    suma2 = suma2 + (GAIN_FLEX*analogRead(myoware2));
    i++;
    delay(10);
  }
  digitalWrite(ledr,LOW);

  //
  // ---------- Determinacion flexión máxima ----------
  //
  Fmax=(suma2)/500;       
 
  //
  // ---------- Reset de variables auxiliares ----------
  //
  i=0;
  suma2=0;

  //
  // -------------------------------------------------------------------- DERIVADA --------------------------------------------------------------------
  //
  // En esta estructura se toman 150 muestras de la señal de FLEXION durante una contracción rápida y se guarda el valor de la máxima derivada //
  // Este proceso se realiza tres veces y se toma una media para calcular el umbral //

  //-------------------------------------------------------------------
  //-------------------- PRIMER CONTRACCIÓN RAPIDA --------------------
  //-------------------------------------------------------------------
  Serial.println(F("Mire la luz azul. Al tercer encendido hacer la flexión rápida. Este procedimiento se realizará tres veces"));
  delay(4000);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  
  while (i<150){ 
    flex=GAIN_FLEX*analogRead(myoware2);
    ta=millis();
    derivada=(flex-flex0)/(ta-t0);  
    V1[i]=derivada;
    i++;
    flex0=flex;
    delay(15);
    t0=ta; 
  }
  
  digitalWrite(leda,LOW);
  i=0;

  while (i<150) 
  {
    if (V1[i]>aux_derivada) //Toma el valor máximo de las 150 muestras
    {
      aux_derivada=V1[i];
      derivadamax[0]=V1[i];
    }
    i++;
  }

  //
  // ---------- Reset de variables auxiliares ----------
  //
  i=0;
  aux_derivada=0;

  //--------------------------------------------------------------------
  //-------------------- SEGUNDA CONTRACCIÓN RAPIDA --------------------
  //--------------------------------------------------------------------
  
  delay(2000);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  
  while (i<150){ 
    flex=GAIN_FLEX*analogRead(myoware2);
    ta=millis();
    derivada=(flex-flex0)/(ta-t0);  
    V2[i]=derivada;
    i++;
    flex0=flex;
    delay(15);
    t0=ta; 
  }
  
  digitalWrite(leda,LOW);
  i=0;
  
  while (i<150)
  {
    if (V2[i]>aux_derivada) //Toma el valor máximo de las 150 muestras
    {
      aux_derivada=V2[i];
      derivadamax[1]=V2[i];
    }
    i++;
  }
  
  //
  // ---------- Reset de variables auxiliares ----------
  //
  i=0; 
  aux_derivada=0;
  
  // --------------------------------------------------------------------
  // -------------------- TERCERA CONTRACCIÓN RAPIDA --------------------
  // --------------------------------------------------------------------
  
  delay(2000);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  digitalWrite(leda,LOW);
  delay(500);
  digitalWrite(leda,HIGH);
  delay(500);
  
  while (i<150){ 
    flex=GAIN_FLEX*analogRead(myoware2);
    ta=millis();
    derivada=(flex-flex0)/(ta-t0);  
    V3[i]=derivada;
    i++;
    flex0=flex;
    delay(15);
    t0=ta; 
  }
  
  digitalWrite(leda,LOW);
  i=0;
  
  while (i<150){
    
    if (V3[i]>aux_derivada) //Toma el valor máximo de las 150 muestras
    {
      aux_derivada=V3[i];
      derivadamax[2]=V3[i];
    }
    i++;
  }
  //
  // ---------- Reset de variables auxiliares ----------
  //
  i=0;
  aux_derivada=0;

  //--------------------------------------------------------------------------
  //-------------------- Determinación de umbral derivada --------------------
  //--------------------------------------------------------------------------

  umbralDerivada=((derivadamax[0]+derivadamax[1]+derivadamax[2])/3)*0.7;

  delay(2000);
  
  // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // ----------------------------------------------------------------- FINAL DE SETUP -------------------------------------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  Serial.print(F("El valor de la extension maxima es: "));
  Serial.println(Emax);
  Serial.print(F("El valor de la flexion maxima es: "));
  Serial.println(Fmax);  
  Serial.print(F("El valor pico de la derivada es: "));
  Serial.println((derivadamax[0]+derivadamax[1]+derivadamax[2])/3);

  delay(3000);
}


void loop() 
{  
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------- FUNCIONAMIENTO GENERAL ---------------------------------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // -------------------------------------------------------------------
  // -------------------- Lectura de valores de EMG --------------------
  // -------------------------------------------------------------------
  ext = GAIN_EXT * analogRead(myoware1);
  flex = GAIN_FLEX * analogRead(myoware2);
  nivelExt = ext/Emax;
  nivelFlex = flex/Fmax;

  /*
  Serial.print(nivelExt);
  Serial.print("\t");
  Serial.println(nivelFlex);
  */
  
  // ----------------------------------------------------------------
  // -------------------- Cálculo de la derivada --------------------
  // ----------------------------------------------------------------
  ta = millis();
  derivada = (flex-flex0)/(ta-t0);
  flex0 = flex; //Valores usados para calcular la derivada
  t0 = ta; //Valores usados para calcular la derivada

  // --------------------------------------------------------------------
  // -------------------- Determinación del comando --------------------- 
  // --------------------------------------------------------------------
  if (derivada > umbralDerivada) // Switch Modo de  Funcionamiento //
  { 
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    interruptor_led=!interruptor_led;
    digitalWrite (leda, interruptor_led);
    
    if (interruptor_modo < 2){
      interruptor_modo++;
    }
    else if (interruptor_modo == 2){
      interruptor_modo = 0; 
    }
    // Serial.print(F("Interruptor: "));
    
    delay(500);
    ta = millis();
  }     
  else if (nivelExt > UMBRAL_EXT) // Extension (salida = -1) //
  {     
    salidaExt++;          
    if(salidaExt >= 3) // Se envia Flexion cuando (nivelExt > UMBRAL_EXTS) en 2 loops consecutivos.
    {  
      // Serial.print(F("Extension"));
      salida_EMG = -1;
      digitalWrite (ledv,HIGH);  
      digitalWrite (ledr,LOW);
    }
  }           
  else if(nivelFlex > UMBRAL_FLEX) // Flexion (salida = 1) //
  {
    salidaFlex++;
    if(salidaFlex >= 3) // Se envia Flexion cuando (nivelFlex > UMBRAL_FLEX) en 2 loops consecutivos.
    {  
      // Serial.print(F("Flexion"));
      salida_EMG = 1;
      digitalWrite (ledr,HIGH);  
      digitalWrite (ledv,LOW);
    }
  }
  else // En caso de no tener actividad, mantengo los motores apagados //
  {  
    //Serial.print(F("Reposo"));
    digitalWrite (ledr,LOW);
    digitalWrite (ledv,LOW);
    salidaExt = 0;
    salidaFlex = 0;
    salida_EMG = 0;
  }

  // ------------------------------------------------------------------------
  // -------------------- Lectura de valores de joystick --------------------
  // ------------------------------------------------------------------------
  joy_x = analogRead(joystick1);
  joy_y= analogRead(joystick2);

  if (joy_x > 800 && joy_x < 1050){
    sal_x = 1;
  }
  else if (joy_x >= 0 && joy_x < 300){
    sal_x = -1;
  }
  else{
    sal_x = 0;
  }

  if (joy_y > 800 && joy_y < 1050){
    sal_y = 1;
  }
  else if (joy_y >= 0 && joy_y < 300){
    sal_y = -1;
  }
  else{
    sal_y = 0;
  }
  
  /*
  Serial.print("\t");
  Serial.print(sal_x);
  Serial.print("\t");
  Serial.println(sal_y);
  */

  // ------------------------------------------------------------------------
  // ---------------------- Creación del paquete JSON -----------------------
  // ------------------------------------------------------------------------
  String mensaje_json;
  StaticJsonDocument<50> doc;
  doc["Val_x"] = sal_x;
  doc["Val_y"] = sal_y;
  doc["Val_EMG"] = salida_EMG;
  doc["Val_Modo"] = interruptor_modo;
  serializeJson(doc, mensaje_json);
  delay(10);

  // ----------------------------------------------------------------------------------------  
  // ---------------------- Envío del mensaje por comunicación serial -----------------------
  // ----------------------------------------------------------------------------------------
  Serial.println(mensaje_json);
  delay(10);      

  // ----------------------------------------------------------------------------------------
  // --------------------------------- Grabar en memoria SD ---------------------------------
  // ----------------------------------------------------------------------------------------
  dataString = "";
  dataString = String(nivelExt) + "\t" + String(nivelFlex); //preparar datos
  saveData(dataString,nombreArchivo);
}

// Funcion que inicia un nuevo archivo
void intializeFile (String fileName)
{
  File dataFile = SD.open(fileName, FILE_WRITE); //Abrir archivo
  if (dataFile) 
  {
    dataFile.println("Data EMG");
    dataFile.println(String("Ext Max") + "\t" + String(Emax));
    dataFile.println(String("Flex Max") + "\t" + String(Fmax));
    dataFile.close();
    Serial.println(F("Inicializando archivo"));// inicializar
  }
  else // if the file isn't open, pop up an error:
  { 
    Serial.print(F("Error initializing "));
    Serial.println(fileName);
  }
}

// Funcion que graba los datos en la memoria SD
void saveData(String dataString, String fileName)
{
  File dataFile = SD.open(fileName, (O_APPEND | O_WRITE)); //Abrir archivo
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);// print to the serial port too:
  }
  else // if the file isn't open, pop up an error:
  { 
    Serial.print(F("error saving to "));
    Serial.println(fileName);
  }
}
  
