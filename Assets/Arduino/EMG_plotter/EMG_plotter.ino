#define GAIN_EXT 1
#define GAIN_FLEX 2.5

float ext = 0;
float flex = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  ext=GAIN_EXT * ( analogRead(A0)) ; //Derecho. AMARILLO/NARANJA
  flex=GAIN_FLEX * ( analogRead(A1)); //Izquierdo NARANJA/BLANCO

  Serial.print(ext);
  Serial.print(" ");
  Serial.println(flex);  

}
